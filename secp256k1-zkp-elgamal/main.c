//
//  main.c
//  secp256-zkp-elgamal
//
//  Created by 宋赓 on 2019/7/26.
//  Copyright © 2019 宋赓. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "include/secp256k1.h"
#include "include/secp256k1_elgamal.h"
#include "include/secp256k1_bulletproofs.h"

static const size_t MAX_PROOF_SIZE = 700;//5134;
static const size_t SCRATCH_SPACE_SIZE = 256 * (1 << 20);
static const size_t MAX_GENERATORS = 256;

secp256k1_scratch_space* _get_scratch_space(const secp256k1_context* ctx) {
    static secp256k1_scratch_space *scratch = NULL;
    static int inited = 0;
    if (!inited) {
        scratch = secp256k1_scratch_space_create(ctx, SCRATCH_SPACE_SIZE);
    }
    return scratch;
}

secp256k1_bulletproof_generators* _get_bp_generators(const secp256k1_context* ctx) {
    static secp256k1_bulletproof_generators *generators = NULL;
    static int inited = 0;
    if (!inited) {
        generators = secp256k1_bulletproof_generators_create(ctx, &secp256k1_generator_const_g, MAX_GENERATORS);
    }
    return generators;
}

int prove_bulletproof(const secp256k1_context* ctx, unsigned char * proof, size_t *plen, uint64_t value, const unsigned char *blind) {
    return secp256k1_bulletproof_rangeproof_prove(ctx, _get_scratch_space(ctx), _get_bp_generators(ctx), proof, plen, NULL, NULL, NULL, &value, NULL, &blind, NULL, 1, &secp256k1_generator_const_h, 64, blind, NULL, NULL, 0, NULL);;
}

int verify_bulletproof(const secp256k1_context* ctx, const secp256k1_pedersen_commitment *commit, const unsigned char * proof, size_t plen) {
    return secp256k1_bulletproof_rangeproof_verify(ctx, _get_scratch_space(ctx), _get_bp_generators(ctx), proof, plen, NULL, commit, 1, 64, &secp256k1_generator_const_h, NULL, 0);
}

void encrypt_decrypt_example(secp256k1_context* ctx) {
    unsigned char seckey[32] = { 0 };
    secp256k1_pubkey pubkey;

    // 1. create sk, pk
    for (int i = 0; i < 32; i++) {
        seckey[i] = (rand() % 255);
    }

    if (!secp256k1_ec_pubkey_create(ctx, &pubkey, seckey)) {
        printf("create ec public key failed.");
        return;
    }

    // 2. encrypt
    unsigned int v_in = 10;//rand();
    const unsigned char rr[] = {0xE1,0x9D,0x72,0x00,0x00,0x00,0x00,0x00,0x60,0x0B,0x2F,0xE5,0x0B,0x72,0xAA,0x1D,0xEC,0x86,0xD3,0xBF,0x1F,0xBE,0x47,0x1B,0xE8,0x98,0x27,0xE1,0x9D,0x72,0xAA,0x1D};
    unsigned char es[128] = {0};
    secp256k1_elgamal_encrypt(ctx, es, &pubkey, rr, v_in);    // es is encrypted ciphertext

    // 3. decrypt
    unsigned char v_out[64] = { 0 };
    secp256k1_elgamal_decrypt(ctx, v_out, es, seckey);

    // 4. find v
    unsigned int v;
    secp256k1_elgamal_find_v(ctx, &v, v_out);
    printf("v = %d\n", v);

    // 5. test adding Homomorphism
    {
        unsigned char homo_out[128] = { 0 };
        secp256k1_elgamal_add(ctx, homo_out, es, es);

        // 5.1 decrypt homo_out
        unsigned char local_v_out[64] = { 0 };
        secp256k1_elgamal_decrypt(ctx, local_v_out, homo_out, seckey);

        // 5.2 find v, v should equal v_in + v_in.
        unsigned int local_v;
        secp256k1_elgamal_find_v(ctx, &local_v, local_v_out);

        printf("local_v = %d\n", local_v);
    }
}

#include "hash.h"
#include "hash_impl.h"

unsigned char prikey_a[32] = { 0x38,0xad,0xe6,0x3a,0xf8,0x81,0x30,0x6d,0x06,0x50,0x45,0x9d,0x99,0xa7,0x5a,0x6f,0x6e,0x9b,0xb9,0x4b,0x13,0x6a,0x15,0x1c,0x60,0x4f,0x4a,0x27,0xb7,0x93,0x03,0x05 };
unsigned char prikey_b[32] = { 0xcf,0x8e,0x33,0x64,0x13,0x2a,0xe0,0x7f,0xc7,0x7a,0x16,0xe7,0x30,0x14,0x7d,0x69,0x3d,0x3e,0x2d,0xb6,0xbd,0xeb,0xd1,0xd6,0x48,0x54,0x6f,0x5e,0x26,0x9c,0x4a,0x92 };
const char *pubkey_a = "029b3a8c13326704fd826c9cdb432431c7427e64845d87d3b0cbf8fa1d27d832ea";
const char *pubkey_b = "02a8d88e11c000a617b321d7adbe687fea04a67c70e07e5b2dfd6b01f627f4c8d5";
const unsigned int origin_a = 100, origin_b = 8, delta = 20, left_a = 80;  // origin balance of A, B, and transfered balance
unsigned char balance_a[128] = { 0x68,0xe2,0x94,0xb9,0x52,0x0c,0xcf,0xde,0xff,0xb6,0xa2,0x09,0x1c,0x40,0x7b,0xeb,0x47,0x7f,0x5b,0xd3,0xcb,0x41,0x1d,0xf4,0x22,0xf2,0x20,0x58,0x9c,0x0e,0xc5,0x7d,0x7b,0xca,0x90,0xaf,0x21,0xf4,0xe6,0x17,0xa8,0x95,0xbe,0x9a,0x12,0xd2,0x25,0x4a,0x77,0x60,0x01,0xbe,0x40,0x63,0xef,0xb3,0x20,0xab,0x53,0x9f,0x32,0x62,0x88,0x3d,0x3c,0xb3,0x79,0xee,0xc4,0xeb,0x69,0xca,0x08,0x62,0x6b,0x0e,0xd3,0xc0,0xff,0xe2,0x37,0xd3,0x7a,0xbd,0x84,0x14,0x1e,0xbb,0x4c,0xf9,0x9d,0x8f,0x39,0x70,0x32,0xc9,0x65,0x65,0xb7,0x2e,0x11,0xa5,0xf5,0x2d,0x33,0xc7,0xa1,0x6b,0x16,0x3b,0x29,0xf4,0x81,0x9b,0x24,0x47,0x1d,0xec,0xea,0xa2,0x70,0x84,0x98,0xcd,0x89,0x97,0x98,0x7c };   // cipher balance of A
unsigned char balance_b[128] = { 0x87,0x86,0xd1,0x9e,0x47,0x40,0x07,0xa7,0x9e,0x9e,0x82,0x76,0xad,0x66,0xbe,0xc6,0x63,0x07,0x17,0x54,0xbc,0x09,0x1c,0x75,0xf4,0xa7,0xf3,0x9d,0xb8,0x87,0xc7,0x31,0x50,0x59,0x78,0x3c,0x82,0xb5,0x22,0xe4,0x66,0x4d,0xd2,0xc6,0xa0,0x7e,0x04,0xd5,0xc6,0x72,0x0c,0x56,0xe6,0x5b,0x95,0x4b,0xd5,0xbb,0xe3,0x2a,0x02,0xc5,0x59,0xcc,0x2b,0x2c,0x90,0x5a,0x45,0x65,0xe8,0x29,0xea,0x17,0x8d,0xa7,0xc1,0x73,0xfa,0x70,0xf8,0x45,0x2e,0x13,0xc2,0xd3,0x29,0xee,0x46,0xc1,0x75,0x3a,0x3d,0xcd,0xe5,0x4c,0xd8,0x82,0x35,0x07,0x68,0x2b,0xd0,0x36,0x6b,0xd2,0xaf,0x08,0xea,0x75,0x06,0xc2,0x53,0x7c,0xd6,0x39,0x23,0x15,0xa7,0xf7,0x73,0x31,0x4e,0x95,0x7c,0x2d,0xda,0x1a };   // cipher balance of B

void log_str(const unsigned char *sz, size_t len)
{
    for (int i = 0; i < len; ++i) {
        printf("%02x", sz[i]);
    }
    printf("\n");
}

char * bin_to_hex(const unsigned char *bin, size_t binsz, char *result, size_t hexsz)
{
    unsigned char hex_str[]= "0123456789abcdef";
    unsigned int i;
    
    if (hexsz < binsz * 2) return NULL;

    if (!binsz) return NULL;
    
    for (i = 0; i < binsz; i++) {
        result[i * 2 + 0] = hex_str[(bin[i] >> 4) & 0x0F];
        result[i * 2 + 1] = hex_str[(bin[i]     ) & 0x0F];
    }
    return result;
}

int get_hex(char c)
{
    if(c >= '0' && c<= '9')
        return c - '0';
    else if(c >= 'a' && c<= 'f')
        return c - 'a' + 10;
    else if(c >= 'A' && c<= 'F')
        return c - 'A' + 10;
    else
        return -1;//error
}

unsigned char* hex_to_bin(const char* hexStr, size_t hexsz, unsigned char * result, size_t binsz)
{
    if (binsz * 2 < hexsz) return NULL;

    unsigned long hexLen = strlen(hexStr);
    for(int i = 0; i < hexLen; i +=2)
    {
        result[i/2] = (get_hex(hexStr[i])*16 + get_hex(hexStr[i+1]));
    }
    
    return result;
}

void roll_random(unsigned char *r, size_t len)
{
    if (len < 32) return;
    for(int i = 0; i < 32; ++i) {
        r[i] = (rand() % 255);
    }
}

void elgamal_test_generate_equal_prove(const secp256k1_context* ctx, char *hexout, size_t len, const unsigned char *r1, size_t rlen1)
{
    // hexout is a 576-byte array that should hold hex-format equal_prove value
    // first 128 byte is ciphertext, last 160 byte is prove
    // 576 = (128 + 160) * 2
    // r1 is a 32-byte random array
    if (len < 576 || rlen1 < 32) return;

    secp256k1_pubkey pubkey_a;
    if (!secp256k1_ec_pubkey_create(ctx, &pubkey_a, prikey_a)) return;

    unsigned char balance_appro[128] = { 0 }; // ca1
    unsigned char eq_prove[160] = { 0 };

    secp256k1_elgamal_encrypt(ctx, balance_appro, &pubkey_a, r1, origin_a);
    secp256k1_elgamal_equal_prove(ctx, eq_prove, balance_a, balance_appro, prikey_a, r1);
    
    bin_to_hex(balance_appro, 128, hexout, 256);
    bin_to_hex(eq_prove, 160, &hexout[256], 320);
}

void elgamal_test_generate_from_valid_prove(const secp256k1_context* ctx, char *hexout, size_t len, const unsigned char *r2, size_t rlen2)
{
    // hexout is a 1990-byte array that should hold hex-format valid_prove value that about sender
    // first 128 byte is ciphertext, second 675 byte is bulletproof, last 192 byte is valid prove
    // 1990 = (128 + 675 + 192) * 2
    // r2 is a 32-byte random array
    
    if (len < 1990 || rlen2 < 32) return;
    
    secp256k1_pubkey pubkey_a;
    if (!secp256k1_ec_pubkey_create(ctx, &pubkey_a, prikey_a)) return;
    
    unsigned char balance_left_a[128] = { 0 }; // ca2
    unsigned char from_valid_prove[192] = { 0 };

    secp256k1_elgamal_encrypt(ctx, balance_left_a, &pubkey_a, r2, left_a);

    unsigned char proof[MAX_PROOF_SIZE] = { 0 };
    size_t plen = MAX_PROOF_SIZE;
    prove_bulletproof(ctx, proof, &plen, left_a, r2);

    secp256k1_elgamal_valid_prove(ctx, from_valid_prove, &pubkey_a, r2, r2, r2, left_a);
    
    bin_to_hex(balance_left_a, 128, hexout, 256);
    bin_to_hex(proof, 675, &hexout[256], 1350);
    bin_to_hex(from_valid_prove, 192, &hexout[256 + 1350], 384);
}

void elgamal_test_generate_to_valid_prove(const secp256k1_context* ctx, char *hexout, size_t len, const unsigned char *r3, size_t rlen3)
{
    // hexout is a 1990-byte array that should hold hex-format valid_prove value that about receiver
    // first 128 byte is ciphertext, second 675 byte is bulletproof, last 192 byte is valid prove
    // 1990 = (128 + 675 + 192) * 2
    // r3 is a 32-byte random array
    
    if (len < 1990 || rlen3 < 32) return;
    
    secp256k1_pubkey pubkey_b;
    if (!secp256k1_ec_pubkey_create(ctx, &pubkey_b, prikey_b)) return;
    
    unsigned char balance_delta[128] = { 0 }; // cb1
    unsigned char to_valid_prove[192] = { 0 };

    secp256k1_elgamal_encrypt(ctx, balance_delta, &pubkey_b, r3, delta);

    unsigned char proof[MAX_PROOF_SIZE] = { 0 };
    size_t plen = MAX_PROOF_SIZE;
    prove_bulletproof(ctx, proof, &plen, delta, r3);

    secp256k1_elgamal_valid_prove(ctx, to_valid_prove, &pubkey_b, r3, r3, r3, delta);
    
    bin_to_hex(balance_delta, 128, hexout, 256);
    bin_to_hex(proof, 675, &hexout[256], 1350);
    bin_to_hex(to_valid_prove, 192, &hexout[256 + 1350], 384);
}

void elgamal_test_generate_ecdsa_signature(const secp256k1_context* ctx, char *hexsig, size_t len,
                                           const char *eqp, size_t len1,
                                           const char *fvp, size_t len2,
                                           const char *tvp, size_t len3,
                                           const unsigned char *r1, size_t rlen1,
                                           const unsigned char *r2, size_t rlen2,
                                           const unsigned char *r3, size_t rlen3)
{
    // hexsig is a 128-byte array
    if (!ctx || len < 128 || len1 < 576 || len2 < 1990 || len3 < 1990) return;

    unsigned char eq_para_sz[288] = { 0 };
    unsigned char from_pub_para_sz[995] = { 0 };
    unsigned char to_pub_para_sz[995] = { 0 };

    hex_to_bin(eqp, 576, eq_para_sz, 288);
    hex_to_bin(fvp, 1990, from_pub_para_sz, 995);
    hex_to_bin(tvp, 1990, to_pub_para_sz, 995);
    
    unsigned char hash[32] = { 0 };
    secp256k1_sha256 hasher;
    secp256k1_sha256_initialize(&hasher);
    secp256k1_sha256_write(&hasher, eq_para_sz, 288);
    secp256k1_sha256_write(&hasher, from_pub_para_sz, 995);
    secp256k1_sha256_write(&hasher, to_pub_para_sz, 995);
    secp256k1_sha256_finalize(&hasher, hash);

    unsigned char seckey[32] = { 0 };
    secp256k1_elgamal_seckey(ctx, seckey, r1, r2,  r3);

    secp256k1_ecdsa_signature signature;
    memset(&signature.data, 0, sizeof(signature.data));
    secp256k1_ecdsa_sign(ctx, &signature, hash, seckey, NULL, NULL);
    
    bin_to_hex(signature.data, 64, hexsig, 128);
}

int elgamal_test_verify_ecdsa_signature(const secp256k1_context* ctx,
                                        const char *eqp, size_t len1,
                                        const char *fvp, size_t len2,
                                        const char *tvp, size_t len3,
                                        const char *sig, size_t len4)
{
    if (!ctx || len1 < 576 || len2 < 1990 || len3 < 1990 || len4 < 128) return 0;

    unsigned char eq_para_sz[288] = { 0 };
    unsigned char from_pub_para_sz[995] = { 0 };
    unsigned char to_pub_para_sz[995] = { 0 };
    
    hex_to_bin(eqp, 576, eq_para_sz, 288);
    hex_to_bin(fvp, 1990, from_pub_para_sz, 995);
    hex_to_bin(tvp, 1990, to_pub_para_sz, 995);

    unsigned char hash[32] = { 0 };
    secp256k1_sha256 hasher;
    secp256k1_sha256_initialize(&hasher);
    secp256k1_sha256_write(&hasher, eq_para_sz, 288);
    secp256k1_sha256_write(&hasher, from_pub_para_sz, 995);
    secp256k1_sha256_write(&hasher, to_pub_para_sz, 995);
    secp256k1_sha256_finalize(&hasher, hash);

    secp256k1_pubkey pub;
    secp256k1_elgamal_pubkey(ctx, &pub, &eq_para_sz[64], &from_pub_para_sz[64], &to_pub_para_sz[64]);

    secp256k1_ecdsa_signature signature;
    memset(&signature.data, 0, sizeof(signature.data));
    hex_to_bin(sig, 128, signature.data, 64);
    return secp256k1_ecdsa_verify(ctx, &signature, hash, &pub);
}

int elgamal_test_verify_equal_prove(const secp256k1_context* ctx, const char *eqp, size_t len1)
{
    if (!ctx || !eqp || len1 < 576) return 0;

    secp256k1_pubkey pb_a;
    unsigned char pub[33] = { 0 };
    hex_to_bin(pubkey_a, 66, pub, 33);
    int ret = secp256k1_ec_pubkey_parse(ctx, &pb_a, pub, 33);
    if (!ret) {
        printf("parse public key A failed\n");
        return ret;
    }
    
    unsigned char eq_para_sz[288] = { 0 };
    hex_to_bin(eqp, 576, eq_para_sz, 288);

    return secp256k1_elgamal_equal_verify(ctx, &eq_para_sz[128], balance_a, eq_para_sz, &pb_a);
}

int elgamal_test_verify_from_valid_prove(const secp256k1_context* ctx, const char *fvp, size_t len2)
{
    if (!ctx || !fvp || len2 < 1990) return 0;
    
    unsigned char from_pub_para_sz[995] = { 0 };
    hex_to_bin(fvp, 1990, from_pub_para_sz, 995);
    
    secp256k1_pedersen_commitment commit;
    secp256k1_bulletproof_string_to_commit(&commit, &from_pub_para_sz[64], 64);
    
    int ret = verify_bulletproof(ctx, &commit, &from_pub_para_sz[128], 675);
    if (!ret) {
        printf("verify bulletproof fvp failed\n");
        return ret;
    }
    
    secp256k1_pubkey pb_a;
    unsigned char pub[33] = { 0 };
    hex_to_bin(pubkey_a, 66, pub, 33);
    ret = secp256k1_ec_pubkey_parse(ctx, &pb_a, pub, 33);
    if (!ret) {
        printf("parse public key A failed\n");
        return ret;
    }
    
    return secp256k1_elgamal_valid_verify(ctx, &from_pub_para_sz[128+675], from_pub_para_sz, &pb_a);
}

int elgamal_test_verify_to_valid_prove(const secp256k1_context* ctx, const char *tvp, size_t len3)
{
    if (!ctx || !tvp || len3 < 1990) return 0;
    
    unsigned char to_pub_para_sz[995] = { 0 };
    hex_to_bin(tvp, 1990, to_pub_para_sz, 995);

    secp256k1_pedersen_commitment commit;
    secp256k1_bulletproof_string_to_commit(&commit, &to_pub_para_sz[64], 64);

    int ret = verify_bulletproof(ctx, &commit, &to_pub_para_sz[128], 675);
    if (!ret) {
        printf("verify bulletproof tvp failed\n");
        return ret;
    }
    
    secp256k1_pubkey pb_b;
    unsigned char pub[33] = { 0 };
    hex_to_bin(pubkey_b, 66, pub, 33);
    ret = secp256k1_ec_pubkey_parse(ctx, &pb_b, pub, 33);
    if (!ret) {
        printf("parse public key B failed\n");
        return ret;
    }
    
    return secp256k1_elgamal_valid_verify(ctx, &to_pub_para_sz[128+675], to_pub_para_sz, &pb_b);
}

void elgamal_test()
{
    // 0. initialize context
    secp256k1_context *ctx = secp256k1_context_create(SECP256K1_CONTEXT_SIGN | SECP256K1_CONTEXT_VERIFY);
    if (!ctx) {
        printf("create context failed.");
        return;
    }
    
    unsigned char r1[32] = { 0 };
    unsigned char r2[32] = { 0 };
    unsigned char r3[32] = { 0 };
    
    char hex_eqp[576 + 1] = { 0 };
    char hex_fvp[1990 + 1] = { 0 };
    char hex_tvp[1990 + 1] = { 0 };
    char hex_sig[128 + 1] = { 0 };
    
    int ret = 0;
    
    roll_random(r1, 32);
    roll_random(r2, 32);
    roll_random(r3, 32);
    
    // Generate
    elgamal_test_generate_equal_prove(ctx, hex_eqp, 577, r1, 32);
    elgamal_test_generate_from_valid_prove(ctx, hex_fvp, 1990, r2, 32);
    elgamal_test_generate_to_valid_prove(ctx, hex_tvp, 1990, r3, 32);

    elgamal_test_generate_ecdsa_signature(ctx, hex_sig, 128, hex_eqp, 576, hex_fvp, 1990, hex_tvp, 1990, r1, 32, r2, 32, r3, 32);
    
    // Verify
    ret = elgamal_test_verify_ecdsa_signature(ctx, hex_eqp, 576, hex_fvp, 1990, hex_tvp, 1990, hex_sig, 128);
    printf("verify signature: %d\n", ret);
    
    ret = elgamal_test_verify_equal_prove(ctx, hex_eqp, 576);
    printf("verify equal prove: %d\n", ret);
    
    ret = elgamal_test_verify_from_valid_prove(ctx, hex_fvp, 1990);
    printf("verify from valid prove: %d\n", ret);
    
    ret = elgamal_test_verify_to_valid_prove(ctx, hex_tvp, 1990);
    printf("verify to valid prove: %d\n", ret);
}

int main(void) {
    srand((unsigned int)time(NULL));
    
    elgamal_test();
    return 0;
}
